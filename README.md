# Khem Labs - Nginx Proxy

Docker image for khemlabs Nginx Proxy

# Install

git clone https://bitbucket.org/khemlabs/khemlabs-nginx-proxy.git

cd khemlabs-nginx-proxy

docker build -t khemlabs/nginx-proxy .

# Run Khemlabs - Site

docker run --name redis -d redis

docker run -d --name khemlabsite --link redis:redishost -p 3001:3000 -v PATH_TO_GIT_CLONE:/space/webapps/khemlabs-site/ khemlabs/site

docker run -d --name nginx-proxy -p 80:80 --link khemlabsite:khemlabsite khemlabs/nginx-proxy

# Run APP in browser

http://khemlabs.com/

# Dependencies

Docker "redis"
Docker "khemlabsite"
